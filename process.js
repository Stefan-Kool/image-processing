const fs = require('fs');
const path = require('path');
const sharp = require('sharp');

const sourcePath = "./source/";
const outputPath = "./output/";

const outputHeight = [ 60, 76, 144, 224, 480, 960, 1140 ];
const pixelRatio = [ 1, 1.3, 1.5, 1.6, 2 ];
const imageType = [ 'jpg', 'webp' ]

const counters = sharp.counters();

let sourceFiles = [];

// File processing
function processFile ( file, ratio, height ) {
	height.forEach( ( height ) => {
		// Get name from file
		const name = file.replace(/\.[^/.]+$/, "");

		// Generate from pixelRatio
		ratio.forEach( ( ratio ) => {
			// Generate JPG
			sharp(sourcePath+file)
				.jpeg({
			    	quality: 65,
		    		chromaSubsampling: '4:4:4'
			  	})
				.resize( null, Math.ceil( height * ratio ) )
				.toFile(`${outputPath}h${height}_x${ratio}_${name}.jpg`)
		  		.then(console.log(`Processed h${height}_x${ratio}_${name}.jpg`))
		  		.catch(err => {`Process error h${height}_x${ratio}_${name}.jpg`});

		  	// Generate WebP
			sharp(sourcePath+file)
				.webp({ 
			    	quality: 65,
					Lossless: false 
				})
				.resize( null, Math.ceil( height * ratio ) )
				.toFile(`${outputPath}h${height}_x${ratio}_${name}.webp`)
		  		.then(console.log(`Processed h${height}_x${ratio}_${name}.webp`))
		  		.catch(err => {`Process error h${height}_x${ratio}_${name}.webp`});
		});
  	});
}

// Batch processing
function processBatch( files ) {
	files.forEach( ( file, index ) => { 
		processFile( file, pixelRatio, outputHeight );
	});
}

// Build a list of files
fs.readdir( sourcePath, function( err, files ) {
	if ( err ) {
		console.error( "Could not list the directory.");
	}
	if ( files ) { 
		sourceFiles = files;
	}
	// Init resize
	processBatch(sourceFiles);
});